package com.sapient.feecalculator;

import com.sapient.feecalculator.constant.FILE_TYPE;
import com.sapient.feecalculator.factory.FileReaderManager;
import com.sapient.feecalculator.model.Transaction;
import com.sapient.feecalculator.service.IFileReaderService;
import com.sapient.feecalculator.service.TrasactionFeeCalculation;
import com.sapient.feecalculator.utils.Utils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Tarun Singh on 3/24/2018.
 */
public class MainApplication {

     public static void main(String arg[]) throws IOException, InvalidFormatException {
         String fileName="SampleData.csv";
         IFileReaderService service=FileReaderManager.createFileReaderInstance(FILE_TYPE.CSV);
         List<Transaction> transactions=service.readFile(fileName);
         TrasactionFeeCalculation feeCalculation=new TrasactionFeeCalculation();
         List<Transaction> transactionList=new ArrayList<>();
         for(int i=0;i<transactions.size();i++){
             transactionList.add(feeCalculation.calculateTransactionFee(transactions.get(i),transactions));
         }
         Collections.sort(transactionList,(transaction1, transaction2) -> {
             int value1 = transaction1.getClientId().compareTo(transaction2.getClientId());
             if (value1 == 0) {
                 int value2 = transaction1.getTransactionType().compareTo(transaction2.getTransactionType());
                 if (value2 == 0) {
                     int value3 = transaction1.getTransactionDate().compareTo(transaction2.getTransactionDate());
                     if (value3 == 0) {
                         return transaction1.getPriority().compareTo(transaction2.getPriority());
                     } else {
                         return value3;
                     }
                 } else {
                     return value2;
                 }
             }
             return value1;
         });

         System.out.println("Client Id | Transaction Type | Transaction Date | Priority | Processing Fee    |");
         transactionList.forEach(transaction -> {
             System.out.println();
             System.out.println(transaction.getClientId()+"\t| "+ Utils.getTypeName(transaction.getTransactionType())+"\t| "+
                     transaction.getTransactionDate()+"\t| "+(transaction.getPriority()?"Y\t":"N")+ "\t| "+
                     transaction.getTransactionFees()+"\t|");
         });
         transactions.clear();
     }
}
