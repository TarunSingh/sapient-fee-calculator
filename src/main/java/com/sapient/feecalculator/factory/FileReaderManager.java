package com.sapient.feecalculator.factory;

import com.sapient.feecalculator.constant.FILE_TYPE;
import com.sapient.feecalculator.service.IFileReaderService;
import com.sapient.feecalculator.service.SingltonFileReaderService;

/**
 * Created by Tarun Singh on 3/24/2018.
 */
public class FileReaderManager {

    public static IFileReaderService createFileReaderInstance(FILE_TYPE fileType) {
        switch (fileType) {
            case CSV:
                return SingltonFileReaderService.getFileReaderInstance().readCsvFile();
            case TEXT:
                return SingltonFileReaderService.getFileReaderInstance().readTextFile();
            case EXCEL:
                return SingltonFileReaderService.getFileReaderInstance().readExcelFile();
            case XML:
                return SingltonFileReaderService.getFileReaderInstance().readXmlFile();
            default:
                return null;
        }
    }
}
