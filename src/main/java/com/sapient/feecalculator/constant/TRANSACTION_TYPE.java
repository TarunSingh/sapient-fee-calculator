package com.sapient.feecalculator.constant;

/**
 * Created by Tarun Singh on 3/22/2018.
 */
public enum TRANSACTION_TYPE {
    BUY("BUY",1),SELL("SELL",2),DEPOSIT("DEPOSITE",3),WITHDRAW("WITHDRAW",4);
    private int type;
    private String name;
    TRANSACTION_TYPE(String name, int type) {
        this.name = name;
        this.type = type;
    }
    public int getType() {
        return type;
    }
    public String getName(){
        return  name;
    }
}
