package com.sapient.feecalculator.constant;

/**
 * Created by Tarun Singh on 3/22/2018.
 */
public enum TRANSACTION_FEES {
    FIVE_HUNDREAD(500),HUNDREAD(100),FIFTY(50),TEN(10);
    private double fees;
    TRANSACTION_FEES(double fees) {
        this.fees = fees;
    }
    public double getFees() {
        return fees;
    }
}
