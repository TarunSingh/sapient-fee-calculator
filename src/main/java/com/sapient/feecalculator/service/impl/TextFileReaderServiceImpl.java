package com.sapient.feecalculator.service.impl;

import com.sapient.feecalculator.model.Transaction;
import com.sapient.feecalculator.service.IFileReaderService;

import java.io.IOException;
import java.util.List;

/**
 * Created by Tarun Singh on 3/24/2018.
 */
public class TextFileReaderServiceImpl implements IFileReaderService {
    public List<Transaction> readFile(String fileName) throws IOException {
        return getTransactionListModel(fileName);
    }

}
