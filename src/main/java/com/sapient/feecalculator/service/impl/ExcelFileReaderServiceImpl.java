package com.sapient.feecalculator.service.impl;

import com.sapient.feecalculator.model.Transaction;
import com.sapient.feecalculator.service.IFileReaderService;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.IOException;
import java.util.List;

/**
 * Created by Tarun Singh on 3/24/2018.
 */
public class ExcelFileReaderServiceImpl implements IFileReaderService {
    public List<Transaction> readFile(String file) throws IOException, InvalidFormatException {
        return getTrasactionListModelXmlFile(file);
    }
}
