package com.sapient.feecalculator.service;

import com.sapient.feecalculator.model.Transaction;
import com.sapient.feecalculator.utils.Utils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Tarun Singh on 3/24/2018.
 */
public interface IFileReaderService {

    public List<Transaction> transactionList = new ArrayList<>();

    public List<Transaction> readFile(String file) throws IOException, InvalidFormatException;

    default public List<Transaction> getTransactionListModel(String fileName) throws IOException {
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach(str -> {
                Transaction transaction = new Transaction();
                String[] transactionAttributes = str.split(",");
                transaction.setExternalTransactionID(transactionAttributes[0]);
                transaction.setClientId(transactionAttributes[1]);
                transaction.setSecurityId(transactionAttributes[2]);
                transaction.setTransactionType(Utils.parseTransactionType(transactionAttributes[3]));
                transaction.setTransactionDate(Utils.parseDate(transactionAttributes[4]));
                transaction.setMarketValue(Utils.parseMarketValue(transactionAttributes[5]));
                transaction.setPriority(Utils.getPriority(transactionAttributes[6]));
                transactionList.add(transaction);
            });

        }

        return transactionList;
    }

    default public List<Transaction> getTrasactionListModelXmlFile(String fileName) throws IOException, InvalidFormatException {
        Workbook workbook = WorkbookFactory.create(new File(fileName));
        Sheet sheet = workbook.getSheetAt(0);
        for (int j = 1; j < sheet.getLastRowNum(); j++) {
            Row row = sheet.getRow(j);
            Transaction transaction = new Transaction();
            for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {

                if (i == 0) {
                    Cell cell = row.getCell(i);
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    transaction.setExternalTransactionID(cell.getStringCellValue());
                } else if (i == 1) {
                    Cell cell = row.getCell(i);
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    transaction.setClientId(cell.getStringCellValue());
                } else if (i == 2) {
                    Cell cell = row.getCell(i);
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    transaction.setSecurityId(cell.getStringCellValue());
                } else if (i == 3) {
                    Cell cell = row.getCell(i);
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    transaction.setTransactionType(Utils.parseTransactionType(cell.getStringCellValue()));
                } else if (i == 4) {
                    Cell cell = row.getCell(i);
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    transaction.setTransactionDate(Utils.parseDate(cell.getStringCellValue()));
                } else if (i == 5) {
                    Cell cell = row.getCell(i);
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    transaction.setMarketValue(Utils.parseMarketValue(cell.getStringCellValue()));
                } else if (i == 6) {
                    Cell cell = row.getCell(i);
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    transaction.setPriority(Utils.getPriority(cell.getStringCellValue()));
                }
            }
            transactionList.add(transaction);

        }

        return transactionList;
    }

}
