package com.sapient.feecalculator.service;

import com.sapient.feecalculator.constant.TRANSACTION_FEES;
import com.sapient.feecalculator.constant.TRANSACTION_TYPE;
import com.sapient.feecalculator.model.Transaction;

import java.util.List;

/**
 * Created by Tarun Singh on 3/24/2018.
 */
public class TrasactionFeeCalculation {

    public Transaction calculateTransactionFee(Transaction transaction, List<Transaction> transactions) {
        if (isIntradayTransaction(transaction, transactions)) {
            transaction.setTransactionFees(TRANSACTION_FEES.TEN.getFees());
        } else {
            if (transaction.getPriority()) {
                transaction.setTransactionFees(TRANSACTION_FEES.FIVE_HUNDREAD.getFees());
            } else {
                if (transaction.getTransactionType() == TRANSACTION_TYPE.SELL.getType() ||
                        transaction.getTransactionType() == TRANSACTION_TYPE.WITHDRAW.getType()) {
                    transaction.setTransactionFees(TRANSACTION_FEES.HUNDREAD.getFees());
                } else if (transaction.getTransactionType() == TRANSACTION_TYPE.BUY.getType() ||
                        transaction.getTransactionType() == TRANSACTION_TYPE.DEPOSIT.getType()) {
                    transaction.setTransactionFees(TRANSACTION_FEES.FIFTY.getFees());
                }

            }

        }
        return transaction;
    }

    private boolean isIntradayTransaction(Transaction transaction, List<Transaction> transactions) {
        boolean isIntraDayTransaction = false;
        Transaction temp = null;
        if (transactions.size() > 0) {
            for (Transaction trans : transactions) {
                if (trans.getClientId().equals(transaction.getClientId()) &&
                        trans.getSecurityId().equals(transaction.getSecurityId()) &&
                        trans.getTransactionDate().equals(transaction.getTransactionDate())) {
                    if ((trans.getTransactionType() == TRANSACTION_TYPE.BUY.getType() &&
                            transaction.getTransactionType() == TRANSACTION_TYPE.SELL.getType()) ||
                            (trans.getTransactionType() == TRANSACTION_TYPE.SELL.getType() &&
                                    transaction.getTransactionType() == TRANSACTION_TYPE.BUY.getType())) {
                        isIntraDayTransaction = true;
                        temp = trans;
                        break;
                    }
                }

            }

        } else {
            isIntraDayTransaction = false;
        }

        return isIntraDayTransaction;
    }
}
