package com.sapient.feecalculator.service;

import com.sapient.feecalculator.service.impl.CsvFileReaderServiceImpl;
import com.sapient.feecalculator.service.impl.ExcelFileReaderServiceImpl;
import com.sapient.feecalculator.service.impl.TextFileReaderServiceImpl;
import com.sapient.feecalculator.service.impl.XmlFileReaderServiceImpl;

/**
 * Created by Tarun Singh on 3/24/2018.
 */
public class SingltonFileReaderService {

    private  static volatile SingltonFileReaderService fileReaderService;
    private static ExcelFileReaderServiceImpl excelFileReaderService;
    private static CsvFileReaderServiceImpl csvFileReaderService;
    private static TextFileReaderServiceImpl textFileReaderService;
    private static XmlFileReaderServiceImpl xmlFileReaderService;

    public static SingltonFileReaderService getFileReaderInstance() {
        if (null == fileReaderService) {
            synchronized (SingltonFileReaderService.class){
                if (null == fileReaderService) {
                    excelFileReaderService = new ExcelFileReaderServiceImpl();
                    csvFileReaderService= new CsvFileReaderServiceImpl();
                    textFileReaderService = new TextFileReaderServiceImpl();
                    fileReaderService = new SingltonFileReaderService();
                    xmlFileReaderService=new XmlFileReaderServiceImpl();
                }
            }
        }
        return fileReaderService;
    }
    public ExcelFileReaderServiceImpl readExcelFile(){
        return excelFileReaderService;
    }
    public CsvFileReaderServiceImpl readCsvFile(){
        return csvFileReaderService;
    }
    public TextFileReaderServiceImpl readTextFile(){
        return textFileReaderService;
    }
    public XmlFileReaderServiceImpl readXmlFile(){return xmlFileReaderService;};
}
